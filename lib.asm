section .text

%define EXIT 60
%define STDIN 0
%define STDOUT 1
%define STDERR 2
%define NEXT_LINE 0xA
%define WHITESPACE 0x20
%define TAB 0x9

global exit
global string_length
global print_char
global print_string
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_message
global read_string
global print_exception

; Принимает код возврата и завершает текущий процесс
exit: ; args = None; return None
    mov rax, EXIT ; 'exit' syscall number
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:  ; args = rdi; return rax
    xor rax, rax ; i=0
    .loop:                     ; while(true) {
        cmp byte[rax + rdi], 0 ;    if (string[i] == 0)
        je .end                ;        break;
        inc rax                ;    i++;
        jmp .loop              ; }
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:  ; args = rdi; return None
    xor rax, rax     ; len(string) = 0
    push rdi         ; save args
    call string_length
    pop rdi          ; load args
    mov rdx, rax     ; rdx = len(string)
    mov rsi, rdi     ; rsi = string_address
    mov rax, STDOUT  ; 'write' syscall number
    mov rdi, STDOUT  ; descriptor stdout
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:  ; args = rdi; return None
    push rdi         ; save args
    mov rsi, rsp     ; rsi = string_address
    pop rdi          ; load args
    mov rdx, 1       ; rdx = len(string)
    mov rax, STDOUT  ; 'write' syscall number
    mov rdi, STDOUT  ; descriptor stdout
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:  ; args = None; return None
    mov rdi, NEXT_LINE
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:  ; args = rdi; return None
    mov rax, rdi ; rax = N
    mov r8, 10   ; base
    mov r9, rsp  ; r9 = stack_pointer
    push 0       ; last char in string = '0'
    .loop:                ; do {
        xor rdx, rdx      ;     remainder = 0
        div r8            ;     rax = data // base; rdx(remainder) = data % base
        add rdx, '0'      ;     translate char to ASCII
        dec rsp           ;     --(stack_pointer)
        mov byte[rsp], dl ;     stack[stack_pointer] = rdx
        test rax, rax     ;
        jnz .loop         ; } while (rax != 0)
    mov rdi, rsp ; write address for first char
    call print_string
    mov rsp, r9  ; restore stack_pointer
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:  ; args = rdi; return None
    cmp rdi, 0         ; if (N >= 0) {
    jge print_uint     ;    print_uint(N)
    jge .end           ;    return
    .negative:         ; } else {
        push rdi       ;    save N value
        mov rdi, '-'   ;    print_char('-')
        call print_char
        pop rdi        ;
        neg rdi        ;    N = -N
        jmp print_uint ;    print_uint(abs(N))
    .end:              ; }
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:  ; args = rdi, rsi; return rax
    xor rcx, rcx  ; i = 0
    xor r8, r8
    xor r9, r9
    .loop:                       ; do {
        mov r8b, byte[rdi + rcx] ;      r8 = string_first[i]
        mov r9b, byte[rsi + rcx] ;      r9 = string_second[i]
        cmp r8b, r9b             ;      if (string_first[i] != string_second[i]) {
        jne .false               ;          return false
        inc rcx                  ;      } i++
        test r8b, r8b            ;
        jnz .loop                ; } while (string_first[i] != 0)
    mov rax, 1      ; return true
    ret
    .false:
        mov rax, 0  ; return false
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:  ; args = None; return rax
    mov rdx, 1      ; rdx = len(string)
    mov rax, STDIN  ; 'input' syscall number
    mov rdi, STDIN  ; descriptor stdin
    push 0          ; push zero char
    mov rsi, rsp    ; load address char
    syscall
    pop rax         ; load char from stack in rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:  ; args = rsi, rdi; return rax, rdx
    xor rax, rax
    xor rcx, rcx
    ; rsi - buffer size
    ; rdi - address start string
    .loop:                   ; while (true) {
        cmp rcx, rsi         ;  if (i > len(buffer))
        jge .exception       ;      exception()
        push rcx             ;  save rcx, rsi, rdi in stack
        push rsi
        push rdi
        call read_char       ;  rax = read_char()
        pop rdi
        pop rsi
        pop rcx              ;  load rcx, rsi, rdi from stack

        test rax, rax        ;  if (rax == 0)
        jz .end              ;      break

        cmp rax, WHITESPACE  ;  if (word[i] == ' ')
        je .skip_whitespace  ;      skip_whitespace()
        cmp rax, TAB         ;  if (word[i] == '  ')
        je .skip_whitespace  ;      skip_whitespace()
        cmp rax, NEXT_LINE   ;  if (word[i] == '\n')
        je .skip_whitespace  ;      skip_whitespace()

        mov [rdi + rcx], rax ;  word[i] = char
        inc rcx              ;  i++
        jmp .loop            ; }
    .exception:
        xor rax, rax  ; return zeros
        xor rdx, rdx
        ret
    .skip_whitespace:
        test rcx, rcx ; if is not beginning of a word
        jnz .end      ;     break
        jmp .loop     ; continue
    .end:
        xor rax, rax
        mov [rdi + rcx], rax ; word[i + 1] = 0 (it's last null)
        mov rdx, rcx         ; return len(word)
        mov rax, rdi         ; and return word_address
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:  ; args = rdi; return rax, rdx
    xor rax, rax
    xor rcx, rcx
    mov r8, 10 ; base
    .loop:                        ; while (true) {
        movzx r9, byte[rdi + rcx] ;     r9 = string[i] (movzx - extend with 0)
        cmp r9, 0                 ;     if (string[i] == 0)
        je .end                   ;         break (it's end string)
        cmp r9b, '0'              ;     if (string[i] < '0')
        jl .end                   ;         break (it's not number)
        cmp r9b, '9'              ;     if (string[i] > '9')
        jg .end                   ;         break
        sub r9b, '0'              ;     number = char(string[i]) - char('0') (in ascii)
        mul r8                    ;     rax = rax * base
        add rax, r9               ;     rax = rax + number
        inc rcx                   ;     i++
        jmp .loop                 ; }
    .end:
        mov rdx, rcx  ; rdx = len(string)
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:  ; args = rdi; return rax, rdx
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], '-'  ; if (string[i] == '-')
    je .negative        ;   negative()
    .positive:
        jmp parse_uint
        ret
    .negative:
        inc rdi         ; i++, skipping '-'
        push rdi        ; save i in stack
        call parse_uint ; parse_uint(i)
        pop rdi         ; load i from stack
        neg rax         ; rax = parsed_num
        inc rdx         ; rdx = len(string)
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:  ; args = rdi; return rax
    push rdi                  ; save args
    call string_length
    pop rdi                   ; load args
    mov r8, rax               ; r8 = len(string)
    cmp rdx, r8               ; if (len(buffer) < len(string))
    jl .exception             ;     exception()
    xor rcx, rcx              ; i = 0
    .loop:                    ; while (true) {
        cmp rcx, r8           ;     if (i > len(string))
        jg .end               ;         break
        mov r9, [rdi + rcx]   ;     r9 = string[i]
        mov [rsi + rcx], r9   ;     buffer[i] = r9
        inc rcx               ;     i++
        jmp .loop             ; }
    .exception:
        mov rax, 0            ; return 0
        ret
    .end:
        mov rax, r8           ; return len(string)
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер строку из stdin, пропуская пробельные символы в начале, строка заканчивается \n.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_string:  ; args = rsi, rdi; return rax, rdx
    xor rax, rax
    xor rcx, rcx
    ; rsi - buffer size
    ; rdi - address start string
    .loop:                   ; while (true) {
        cmp rcx, rsi         ;  if (i > len(buffer))
        jge .exception       ;      exception()
        push rcx             ;  save rcx, rsi, rdi in stack
        push rsi
        push rdi
        call read_char       ;  rax = read_char()
        pop rdi
        pop rsi
        pop rcx              ;  load rcx, rsi, rdi from stack

        cmp rax, 0           ;  if (rax == 0)
        je .end              ;      break

        cmp rax, WHITESPACE  ;  if (word[i] == ' ')
        je .skip_whitespace  ;      skip_whitespace()
        cmp rax, TAB         ;  if (word[i] == '  ')
        je .skip_whitespace  ;      skip_whitespace()
        cmp rax, NEXT_LINE   ;  if (word[i] == '\n')
        je .skip_line_feed   ;      skip_line_feed()
    .save_char:
        mov [rdi + rcx], rax ;  word[i] = char
        inc rcx              ;  i++
        jmp .loop            ; }

    .exception:
        xor rax, rax         ; return zeros
        xor rdx, rdx
        ret
    .skip_line_feed:
        cmp rcx, 0           ; if is not beginning of a word
        jne .end             ;     break
        jmp .loop            ; continue
    .skip_whitespace:
        cmp rcx, 0           ; if is not beginning of a word
        jne .save_char       ;     save_char()
        jmp .loop            ; continue
    .end:
        xor rax, rax
        mov [rdi + rcx], rax ; word[i + 1] = 0 (it's last null)
        mov rdx, rcx         ; return len(word)
        mov rax, rdi         ; and return word_address
        ret

; Принимает указатель на нуль-терминированную строку хранящую сообщение об ошибке, выводит её в stderr
print_exception:  ; args = rdi; return None
    xor rax, rax     ; len(string) = 0
    push rdi         ; save args
    call string_length
    pop rdi          ; load args
    mov rdx, rax     ; rdx = len(string)
    mov rsi, rdi     ; rsi = string_address
    mov rax, STDOUT  ; 'write' syscall number
    mov rdi, STDERR  ; descriptor stderr
    syscall
    ret
