ASM=nasm
ASMFLAGS=-f elf64
LD=ld

%.o: %.asm
	nasm $(ASMFLAGS) $< -o $@

main: main.o dict.o lib.o
	$(LD) -o $@ $^

.PHONY: clean
clean:
	rm *.o main
