global _start

%include "lib.inc"
%include "colon.inc"
%include "words.inc"

extern find_word

%define STDOUT 1
%define POINTER_SIZE 8
%define BUFFER_SIZE 256

section .rodata
    greeting_msg: db "Hi! Type name for key to find value in dictionary", 0
    found_key_msg: db "Your key found!", 0
    exception_size_msg: db "Your key name is too long", 0
    exception_not_found_key_msg: db "Key name not found in dictionary", 0

section .bss
    buffer: resb BUFFER_SIZE

section .text

_start:
    .greeting:
        mov rdi, greeting_msg
        call print_string
        call print_newline
    .checking:
        mov rdi, buffer              ; start buffer
        mov rsi, BUFFER_SIZE         ; set size buffer
        call read_string             ; read word from STDIN; rax = address_word, rdx = len(word)
        test rax, rax                ; if (len(word) > BUFFER_SIZE)
        jz .exception_size           ;     exception_size()

        mov rdi, rax                 ; rdi = key_address
        mov rsi, NEXT_VALUE          ; rsi = address NEXT_VALUE
        push rdx                     ; save register
        call find_word               ; rax = find_word()
        pop rdx                      ; load register
        cmp rax, 0                   ; if (find_word == 0)
        je .exception_not_found_key  ;      exception_not_found_key()
    .printing:
        push rdi
        push rax
        push rdx                     ; save registers
        mov rdi, found_key_msg
        call print_string
        call print_newline
        pop rdx                      ; load registers
        pop rax
        pop rdi

        mov rdi, rax                 ; rdi = address_find_value
        add rdi, POINTER_SIZE        ; rdi += POINTER_SIZE
        add rdi, 1                   ; rdi++
        add rdi, rdx                 ; skipping key
        call print_string            ; print(value)
    .end:
        call print_newline
        call exit

    ; exceptions handling
    .exception_size:
        mov rdi, exception_size_msg
        jmp .end_for_exception
    .exception_not_found_key:
        mov rdi, exception_not_found_key_msg
    .end_for_exception:
        call print_exception
        call print_newline
        mov rdi, 1
        call exit
