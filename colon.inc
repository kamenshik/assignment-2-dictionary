%define NEXT_VALUE 0

%macro colon 2
    %ifid %2
        %2: dq NEXT_VALUE
    %else
        %print_exception "key label not id"
    %endif
    %define NEXT_VALUE %2
    %ifstr %1
        db %1, 0
    %else
        %print_exception "key name value not string"
    %endif
%endmacro
