global find_word

%include "lib.inc"

%define POINTER_SIZE 8

section .text

find_word:  ; args = rdi, rsi; return rax
    ; rdi: pointer at key string; rsi: pointer at start list
    .loop:                     ; do {
        add rsi, POINTER_SIZE  ;    set pointer to the key in dict
        push rdi
        push rsi               ;    save registers
        call string_equals     ;    result_equals = rax = string_equals(rdi, rsi)
        pop rsi                ;    load registers
        pop rdi
        sub rsi, POINTER_SIZE  ;    set pointer to the beginning value

        cmp rax, 1             ;    if (result_equals == 1)
        je .return_address     ;        return_address()

        mov rsi, [rsi]         ;    rsi = address(next_value)
        test rsi, rsi
        jnz .loop              ; } while (pointer != null)
    .return_null:
        xor rax, rax
        ret
    .return_address:
        mov rax, rsi
        ret
